//
//  ImageGallery.swift
//  Image Gallery
//

import Foundation
import UIKit

struct ImageGallery: Equatable {
    var name: String

    var images = [UIImage]()

    init(_ name: String = "Default") {
        self.name = name
    }
}
