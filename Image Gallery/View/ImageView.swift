//
//  ImageView.swift
//  Image Gallery
//
//  Created by paul on 24/03/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

class ImageView: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.minimumZoomScale = 1/10
            scrollView.maximumZoomScale = 1.5
            scrollView.delegate = self
        }
    }

    var image: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
