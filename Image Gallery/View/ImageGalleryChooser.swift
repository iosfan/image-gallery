//
//  ImageGalleryChooser.swift
//  Image Gallery
//

import UIKit

class ImageGalleryChooser: UITableViewController, ImageGalleryDelegate {

    var galleries = [ImageGallery()]

    var deletedGalleries = [ImageGallery]()

    var selectedGalleryIndex: Int = 0
    
    var defaultGalleryIndex: Int {
        return galleries.firstIndex { $0.name == "Default" }!
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        openGallery(galleries.first!)
    }

    @IBAction func addGallery(_ sender: UIBarButtonItem) {
        let names = galleries.map { $0.name }
        galleries.append(ImageGallery("Untitled".madeUnique(withRespectTo: names)))

        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return Section.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Section.getSection(section) == .deleted ? deletedGalleries.count : galleries.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellId = Section.getSection(indexPath.section) == .deleted ? "DeletedGalleryCell" : "ImageGalleryCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)

        if let activeCell = cell as? ImageGalleryCell {
            activeCell.name.text = galleries[indexPath.row].name
            activeCell.resignationHandler = { [weak self, unowned activeCell] in
                guard let self = self else {return}
 
                let newName = activeCell.name.text!
                let names = self.galleries.map { $0.name }
                
                if indexPath.row != self.defaultGalleryIndex, !names.contains(newName) {
                    self.galleries[indexPath.row].name = newName
                }
                tableView.reloadData()
            }
        } else {
            cell.textLabel?.text = deletedGalleries[indexPath.row].name
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if Section.getSection(indexPath.section) == .active {
            openGallery(galleries[indexPath.row])
        }
    }
    
    private func openGallery(_ gallery: ImageGallery) {
        let detailController = storyboard?.instantiateViewController(withIdentifier: "ImageGalleryDetail") as? ImageGalleryDetail
        detailController?.images = gallery.images
        detailController?.delegate = self
        selectedGalleryIndex = galleries.firstIndex(of: gallery) ?? 0

        let navCon = UINavigationController(rootViewController: detailController!)
        splitViewController?.showDetailViewController(navCon, sender: nil)
    }
    
    func updateSelectedGallery(with image: UIImage, at destinationIndex: Int, from sourceIndex: Int? = nil) {
        if galleries.indices.contains(selectedGalleryIndex) {
            if sourceIndex != nil {
                galleries[selectedGalleryIndex].images.remove(at: sourceIndex!)
            }

            galleries[selectedGalleryIndex].images.insert(image, at: destinationIndex)
        }
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }

        tableView.performBatchUpdates({
            let removedGallery = Section.getSection(indexPath.section) == .active
                ? galleries.remove(at: indexPath.row)
                : deletedGalleries.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            
            if Section.getSection(indexPath.section) == .active {
                deletedGalleries.append(removedGallery)
                let newIndexPath = IndexPath(row: deletedGalleries.count - 1, section: Section.getRaw(.deleted))
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        })

        openGallery(galleries.first!)
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Section.getSection(section) == .deleted ? "Recently Deleted" : "Active"
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return Section.getSection(indexPath.section) == .deleted || indexPath.row != defaultGalleryIndex
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
        -> UISwipeActionsConfiguration? {
            
        guard Section.getSection(indexPath.section) == .deleted else { return nil }

        let action = UIContextualAction(style: .normal, title: "Restore", handler: {(action, view, completionHandler) in
            tableView.performBatchUpdates({
                let restoredGallery = self.deletedGalleries.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
                
                self.galleries.append(restoredGallery)
                let newIndexPath = IndexPath(row: self.galleries.count - 1, section: Section.getRaw(.active))
                tableView.insertRows(at: [newIndexPath], with: .fade)
            })
        })
        action.backgroundColor = .green

        return UISwipeActionsConfiguration(actions: [action])
    }
}

extension ImageGalleryChooser {
    private enum Section: Int, CaseIterable {
        case deleted
        case active
        
        static func numberOfSections() -> Int {
            return allCases.count
        }
        
        static func getSection(_ section: Int) -> Section {
            return allCases[section]
        }
        
        static func getRaw(_ section: Section) -> Int {
            return section.rawValue
        }
    }
}
