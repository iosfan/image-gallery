//
//  ImageGalleryCell.swift
//  Image Gallery
//
//  Created by paul on 23/03/2020.
//  Copyright © 2020 Stanford University. All rights reserved.
//

import UIKit

class ImageGalleryCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var name: UITextField! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(beginEditing))
            tap.numberOfTapsRequired = 2
            addGestureRecognizer(tap)

            name.delegate = self
        }
    }
    
    @objc func beginEditing() {
        name.isEnabled = true
        name.becomeFirstResponder()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.isEnabled = false
        textField.resignFirstResponder()
        return true
    }
    
    var resignationHandler: (() -> Void)?
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        resignationHandler?()
    }
}
