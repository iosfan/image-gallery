//
//  ImageCell.swift
//  Image Gallery
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        layoutAttributes.bounds.size.height = layoutAttributes.size.width / (image?.aspectRatio ?? 1)
        return layoutAttributes
    }
}

extension UIImage {
    var aspectRatio: CGFloat {
        return self.size.width / self.size.height
    }
}
