//
//  ImageGalleryDetail.swift
//  Image Gallery
//

import UIKit

class ImageGalleryDetail: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var images = [UIImage?]()
    
    weak var delegate: ImageGalleryDelegate?

    private lazy var emptyStateView: UIView = {
        let backgroundView = UIView()
        let stackView = UIStackView()
        stackView.axis = .vertical
        backgroundView.addSubview(stackView)
        let label = UILabel()
        label.text = "EMPTY CONTENT"
        label.textAlignment = .center
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "photo")
        
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 250),
            imageView.widthAnchor.constraint(equalToConstant: 300),
        ])
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(label)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: backgroundView.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: backgroundView.centerYAnchor),
        ])
        return backgroundView
    }()
    
    @IBOutlet weak var imageCollectionView: UICollectionView! {
        didSet {
            imageCollectionView.dropDelegate = self
            imageCollectionView.dragDelegate = self

            imageCollectionView.addGestureRecognizer(UIPinchGestureRecognizer(target: self, action: #selector(scale)))
        }
    }
    
    @objc func scale(_ gestureRecognizer : UIPinchGestureRecognizer) {
        guard gestureRecognizer.view != nil else { return }
        
        if gestureRecognizer.state == .began || gestureRecognizer.state == .changed {
            scaleFactor *= gestureRecognizer.scale
            gestureRecognizer.scale = 1.0
        }
    }
    
    var scaleFactor: CGFloat = 1.0 {
        didSet {
            flowLayout?.invalidateLayout()
        }
    }

    var flowLayout: UICollectionViewFlowLayout? {
        let layout = imageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout?.sectionInset = .init(
            top: Constants.marginBetweenCells,
            left: Constants.marginBetweenCells,
            bottom: Constants.marginBetweenCells,
            right: Constants.marginBetweenCells
        )

        return layout
    }

    var collectionWidth: CGFloat {
        return (collectionView?.bounds.width)!
    }

    var spaceBetweenItems: CGFloat {
        return (flowLayout?.minimumInteritemSpacing)!
    }

    var sideSpacing: CGFloat {
        return (flowLayout?.sectionInset.left)! + (flowLayout?.sectionInset.right)!
    }

    var collectionViewWidthWithoutSpacings: CGFloat {
        return collectionWidth - (spaceBetweenItems * (Constants.columnsCount - 1.0)) - sideSpacing
    }

    var defaultWidth: CGFloat {
        let width = floor((collectionViewWidthWithoutSpacings / Constants.columnsCount) * scaleFactor)
        return min(max(width, collectionWidth * Constants.minWidthRation), collectionViewWidthWithoutSpacings)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(
            width: defaultWidth,
            height: CGFloat(defaultWidth / (images[indexPath.item]?.aspectRatio ?? 1)).rounded(.towardZero)
        )
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath)

        if let imageCell = cell as? ImageCell {
            imageCell.image = images[indexPath.item]
            
            return imageCell
        }
    
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageController = storyboard?.instantiateViewController(withIdentifier: "ImageView") as? ImageView
        imageController?.image = images[indexPath.item]

        navigationController?.pushViewController(imageController!, animated: true)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        if splitViewController?.preferredDisplayMode != .primaryOverlay {
            splitViewController?.preferredDisplayMode = .primaryOverlay
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if images.isEmpty {
            self.collectionView.backgroundView = self.emptyStateView
        }
        
        flowLayout?.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    }
}

extension ImageGalleryDetail: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        session.localContext = collectionView
        return dragItems(at: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, itemsForAddingTo session: UIDragSession, at indexPath: IndexPath, point: CGPoint) -> [UIDragItem] {
        return dragItems(at: indexPath)
    }
    
    private func dragItems(at indexPath: IndexPath) -> [UIDragItem] {
        if let image = (collectionView.cellForItem(at: indexPath) as? ImageCell)?.imageView.image {
            let dragItem = UIDragItem(itemProvider: NSItemProvider(object: image))
            dragItem.localObject = images[indexPath.item]
            return [dragItem]
        } else {
            return []
        }
    }
}

extension ImageGalleryDetail: UICollectionViewDropDelegate {
    func collectionView(_ collectionView: UICollectionView,
                       canHandle session: UIDropSession) -> Bool {
        let isSelf = (session.localDragSession?.localContext as?
                                     UICollectionView) == collectionView
        if isSelf {
            return session.canLoadObjects(ofClass: UIImage.self)
        } else {
            return session.canLoadObjects(ofClass: NSURL.self) && session.canLoadObjects(ofClass: UIImage.self)
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        dropSessionDidUpdate session: UIDropSession,
        withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {

        let isSelf = (session.localDragSession?.localContext as? UICollectionView) == collectionView
        return UICollectionViewDropProposal(operation: isSelf ? .move : .copy, intent: .insertAtDestinationIndexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        let destinationIndexPath = coordinator.destinationIndexPath ?? IndexPath(item: 0, section: 0)
        for item in coordinator.items {
            if let sourceIndexPath = item.sourceIndexPath {
                internalMove(item, from: sourceIndexPath, to: destinationIndexPath, coordinator)
            } else {
                drop(item: item, to: destinationIndexPath, with: coordinator)
            }
        }
    }
    
    private func drop(item: UICollectionViewDropItem, to destination: IndexPath, with coordinator: UICollectionViewDropCoordinator) {
        let placeholderContext = coordinator.drop(
            item.dragItem,
            to: UICollectionViewDropPlaceholder(insertionIndexPath: destination, reuseIdentifier: "DropPlaceholderCell")
        )
        collectionView.backgroundView?.isHidden = true
        images.insert(nil, at: destination.item)
        
        item.dragItem.itemProvider.loadObject(ofClass: NSURL.self) { [weak self] (provider, error) in
            if let url = provider as? URL {
                self?.fetchImage(by: url) { image in
                    placeholderContext.commitInsertion(dataSourceUpdates: { insertionIndexPath in
                        self?.images[destination.item] = image
                        self?.delegate?.updateSelectedGallery(with: image, at: destination.item)
                    })
                }
            } else {
                placeholderContext.deletePlaceholder()
            }
        }
    }
    
    private func internalMove(
        _ item: UICollectionViewDropItem,
        from source: IndexPath,
        to destination: IndexPath,
        _ coordinator: UICollectionViewDropCoordinator) {
        
        collectionView.performBatchUpdates({
            let image = images.remove(at: source.item)
            images.insert(image, at: destination.item)
            
            collectionView.deleteItems(at: [source])
            collectionView.insertItems(at: [destination])
            delegate?.updateSelectedGallery(with: image!, at: destination.item, from: source.item)
        })
        coordinator.drop(item.dragItem, toItemAt: destination)
    }
    
    private func fetchImage(by url: URL, completeHandler: @escaping (UIImage) -> ()) {
        DispatchQueue.global(qos: .utility).async(execute: {
            let urlContents = try? Data(contentsOf: url.imageURL)
            DispatchQueue.main.async {
                var image = UIImage(systemName: "xmark.octagon")
                if let imageData = urlContents, let uploadedImage = UIImage(data: imageData) {
                    image = uploadedImage
                }

                completeHandler(image!)
            }
        })
    }
}

extension ImageGalleryDetail {
    private struct Constants {
        static let marginBetweenCells: CGFloat = 2
        static let minWidthRation: CGFloat = 0.03
        static let columnsCount: CGFloat = 3.0
    }
}
