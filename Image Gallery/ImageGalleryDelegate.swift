//
//  ImageGalleryDelegate.swift
//  Image Gallery
//

import Foundation
import UIKit

protocol ImageGalleryDelegate: AnyObject {
    func updateSelectedGallery(with image: UIImage, at destinationIndex: Int, from sourceIndex: Int?)
}

extension ImageGalleryDelegate {
    func updateSelectedGallery(with image: UIImage, at destinationIndex: Int) {
        updateSelectedGallery(with: image, at: destinationIndex, from: nil)
    }
}
